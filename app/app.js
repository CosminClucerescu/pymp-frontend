const DEFAULT_GET_COUNT = 100;
const SERVER_ADDRESS = "ws://127.0.0.1:50000";
(function () {
    'use strict';
    angular.module('player', [
        'ngMaterial',
        'ngMessages',
        'ngWebSocket',
        'jkAngularRatingStars'])
        .filter('seconds_to_time', function () {
                return function (seconds) {
                    if (seconds === undefined) {
                        return "00:00";
                    }
                    let hours = Math.floor(seconds / 3600) || 0;
                    let min = (Math.floor(seconds / 60) - hours * 60) || 0;
                    let sec = parseInt(seconds - min * 60 - hours * 3600) || 0;
                    let r_val = min + ':' + (sec < 10 ? '0' : '') + sec;
                    if (hours) {
                        r_val = hours + ':' + r_val;
                    }
                    return r_val;

                };
            }
        ).factory('PympBackend', function ($websocket) {
        // Open a WebSocket connection
        let dataStream = $websocket(SERVER_ADDRESS);

        let pending_commands = [];

        dataStream.onMessage(function (message) {
            message = JSON.parse(message.data);
            if (pending_commands[0].command !== message.command) {
                throw 'My assumption was wrongs stuff is not executed in order';
            }
            let command = pending_commands.shift();
            // TODO: This is a terrible way to implement this but at least it works for now.
            let result = message.result;
            if (message.status === 'success') {
                while (command.callbacks.length > 0) {
                    result = command.callbacks.shift()(result);
                }
                while (command.fail_callbacks.length > 0) {
                    command.fail_callbacks.shift();
                }
            } else {
                console.error(`Command failed`);
                console.error(message);
                while (command.callbacks.length > 0) {
                    command.callbacks.shift();
                }
                while (command.fail_callbacks.length > 0) {
                    command.fail_callbacks.shift()(result);
                }
            }
        });
        return {
            runCommand: function (command_name, parameters = {}) {
                let command = {
                    command: command_name,
                    callbacks: [],
                    fail_callbacks: [],
                    then: function (callback) {
                        this.callbacks.push(callback);
                        return this;
                    },
                    fail: function (callback) {
                        this.fail_callbacks.push(callback);
                        return this;
                    }
                };
                pending_commands.push(command);
                let req_command = {
                    'name': command_name,
                    'parameters': parameters
                };
                dataStream.send(JSON.stringify(req_command));
                return command;

            }
        };
    })
        .service('playlistService', ['PympBackend', function (PympBackend) {
            let playlist = this;
            playlist.topIndex = undefined;
            playlist.play = function (song) {
                playerService.play(song);
            };
            playlist.create_new = function () {
                playerService.create_playlist();
            };
            playlist.query_string = "";
            playlist.song = null;
            playlist.songs = {
                loadedPages: {},
                total: 0,
                PAGE_SIZE: DEFAULT_GET_COUNT,
                getItemAtIndex: function (index) {
                    let pageNumber = Math.floor(index / playlist.songs.PAGE_SIZE);
                    let page = this.loadedPages[pageNumber];
                    if (page) {
                        return page[index % playlist.songs.PAGE_SIZE];
                    } else if (page !== null) {
                        this.fetchPage_(pageNumber);
                    }
                },
                getLength: function () {
                    return playlist.songs.total;
                },
                fetchPage_: function (pageNumber) {
                    playlist.songs.loadedPages[pageNumber] = null;
                    PympBackend.runCommand('playback.get_song_list', {
                        count: DEFAULT_GET_COUNT,
                        page: pageNumber + 1
                    }).then(
                        function (data) {
                            playlist.songs.total = data.total;
                            playlist.songs.loadedPages[pageNumber] = data.song_list;
                        }
                    ).fail(function (data) {
                        pageNumber = 0;
                        playlist.topIndex = 0;
                    });
                },
            };
            playlist.changePlaylist = function () {
                playlist.songs.loadedPages = {};
                playerService.set_playlist();

            };
            playlist.update_query = function () {
                playlist.songs.loadedPages = {};
                PympBackend.runCommand('playback.change_query', {'query': playlist.query_string});
            };
            playlist.updateLastPlayed = function (song) {
                if (!song) {
                    return;
                }
                PympBackend.runCommand('playback.get_song_list', {
                    count: DEFAULT_GET_COUNT,
                    page: 1,
                    'query': `pk:"${song.pk}"`
                }).then(
                    function (response) {
                        for (let page in playlist.songs.loadedPages) {
                            page = playlist.songs.loadedPages[page];
                            for (let i = 0; i < page.length; i++) {
                                if (page[i].pk === song.pk) {
                                    page[i] = response.song_list[0];
                                }
                            }
                        }

                    });
            };
        }])
        .service('playerService', ['PympBackend', '$interval', 'playlistService', function (PympBackend, $interval, playlistService) {
            let service = this;
            this.new_monitored_dir = null;
            service.repeat_modes = ['repeat-one', 'repeat-all', 'no-repeat'];//TODO:Load from server
            service.statuses = {
                'playing': 'playing',
                'paused': 'paused',
                'stopped': 'stopped'
            }; // TODO: Load from backend
            service.monitored_dirs = [];
            let default_playlist = {"name": "Library", "pk": 0};
            service.playlist = default_playlist;
            this.getMonitoredDirs = function () {
                return PympBackend.runCommand('file_system.get_monitored_dirs').then(function (data) {
                    service.monitored_dirs = data;
                });
            };
            this.AddMonitoredDir = function () {
                return PympBackend.runCommand('file_system.add_monitored_dir',
                    {'path': service.new_monitored_dir}).then(function (data) {
                    service.monitored_dirs.push(service.new_monitored_dir);
                    service.new_monitored_dir = '';

                });
            };
            this.RemoveMonitoredDir = function (index) {
                return PympBackend.runCommand('file_system.remove_monitored_dir',
                    {'path': service.monitored_dirs[index]}).then(function (data) {
                    service.monitored_dirs.splice(index, 1);
                });
            };
            this.play_pause = function () {
                return PympBackend.runCommand('playback.play_pause').then(service.updateStatus);
            };
            this.stop = function () {
                return PympBackend.runCommand('playback.stop').then(service.updateStatus);
            };
            this.next = function () {
                return PympBackend.runCommand('playback.play_next').then(service.updateStatus);
            };
            this.previous = function () {
                return PympBackend.runCommand('playback.play_previous').then(service.updateStatus);
            };
            this.changeRepeatMode = function () {
                return PympBackend.runCommand('playback.change_repeat_mode', {new_mode: service.repeat_mode});
            };
            this.play = function (song) {
                return PympBackend.runCommand('playback.play', {'pk': song.pk}).then(service.updateStatus);
            };
            this.get_playlists = function () {
                service.playlists = [default_playlist];
            };
            this.rate_song = function (song) {
                if (song.pk === service.song.pk) {
                    service.song.rating = song.rating;
                }
                if (playlistService.song && song.pk === playlistService.song.pk) {
                    playlistService.song.rating = song.rating;
                }
                return PympBackend.runCommand('core.update_song', {'pk': song.pk, 'rating': song.rating});
            };
            this.changePosition = function () {
                PympBackend.runCommand('playback.set_position', {'position': service.position}).then(function (result) {
                    service.position = result.position;
                });


            };
            this.changeVolume = function () {
                PympBackend.runCommand('playback.set_volume', {'volume': service.volume / 100}).then(function (result) {
                    service.volume = result.volume * 100;
                });
            };
            this.updateStatus = function (data = null) {
                PympBackend.runCommand('playback.get_status').then(function (result) {
                    console.log(result);
                    playlistService.updateLastPlayed(service.song);
                    service.volume = result.volume * 100;
                    service.song = result.song;
                    service.duration = result.duration;
                    service.position = result.position;
                    service.status = result.status;
                    service.repeat_mode = result.repeat_mode;
                    playlistService.query_string = result.playlist_query;
                    service.playlist = service.playlists.find(
                        function (element) {
                            return element.pk === result.playlist_pk;
                        }
                    );
                    service.query_string = result.filter;
                });
            };

            $interval(function () {
                if (service.status !== service.statuses.playing) {
                    return;
                }
                service.position += 0.25;
                if (service.position >= service.duration) {
                    service.position = service.duration;
                    service.updateStatus();
                }
            }, 250);
            return this;

        }])
        .directive('linkToCurrentlyPlaying', ['playerService', 'playlistService', function (playerService, playlistService) {
            return {
                restrict: 'A',
                scope: {
                    linkToCurrentlyPlaying: '='
                },
                link: function ($scope) {
                    $scope.$watch('linkToCurrentlyPlaying', function () {
                        let song = $scope.linkToCurrentlyPlaying;
                        if (song != null && playerService.song != null && song.pk === playerService.song.pk) {
                            playlistService.song = song;
                        }
                    });
                }
            };

        }])
        .controller('PlayerController', ['$scope', '$interval', '$window', 'playerService', 'PympBackend', 'playlistService', '$mdDialog',
            function ($scope, $interval, $window, playerService, PympBackend, playlistService, $mdDialog) {
                let controller = this;
                controller.player = playerService;
                controller.playlist = playlistService;

                $window.onfocus = function () {
                    controller.player.updateStatus();
                    controller.player.getMonitoredDirs();
                };

                controller.player.get_playlists();
                controller.player.updateStatus();
                controller.player.getMonitoredDirs();
                controller.playlist.songs.fetchPage_(0);

                $scope.showAdvanced = function (ev) {
                    $mdDialog.show({
                        controller: DialogController,
                        templateUrl: 'dialog1.tmpl.html',
                        parent: angular.element(document.body),
                        targetEvent: ev,
                        clickOutsideToClose: true
                    });
                };

                function DialogController($scope, $mdDialog) {
                    $scope.player = controller.player;
                    $scope.controller = controller;
                    $scope.monitored_dirs = controller.player.monitored_dirs;
                    $scope.AddMonitoredDir = function () {
                        controller.player.AddMonitoredDir();
                        // .then(function (data) {
                        //     $scope.monitored_dirs = controller.player.monitored_dirs;
                        // });
                    };
                    $scope.RemoveMonitoredDir = function (index) {
                        controller.player.RemoveMonitoredDir(index);
                        //     .then(function (data) {
                        //     $scope.monitored_dirs = controller.player.monitored_dirs;
                        // });
                    };

                }
            }
        ]);
})
();


/**
 Copyright 2018 Google Inc. All Rights Reserved.
 Use of this source code is governed by an MIT-style license that can be foundin the LICENSE file at http://material.angularjs.org/HEAD/license.
 **/